import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ActivityIndicator,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header}>
                    <TouchableOpacity style={{ alignSelf: 'center' }}>
                        <Icon name="menu" size={25} color="#fff" style={{ marginLeft: 15 }} />
                    </TouchableOpacity>
                    <Image source={require('../assets/marvel-logo.png')} style={{ width: 122, height: 44, alignItems: 'center' }} />
                    <TouchableOpacity style={{ alignSelf: 'center' }}>
                        <Icon name="search" size={25} color="#fff" style={{ marginRight: 15 }} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.content}>
                        <Text style={styles.title}>The Amazing Spider-Man (2018) #1</Text>
                        <Image style={styles.imageContent} source={require('../assets/theamazingspiderment.jpg')} />
                        <Text style={styles.judulContent}>Published:</Text>
                        <Text style={styles.isi}>Juli 11, 2018</Text>
                        <Text style={styles.judulContent}>Writter:</Text>
                        <Text style={styles.isi}>Nick Spencer</Text>
                        <Text style={styles.judulContent}>Penciler:</Text>
                        <Text style={styles.isi}>Ryan Ottley, Humberto Ramos </Text>
                        <Text style={styles.judulContent}>Cover Artist:</Text>
                        <Text style={styles.isi}>Ryan Ottley </Text>
                        <Text style={styles.detail}>An alien invasion hits New York City and the only one who can stop it is…Spider-Man?!  And if even that’s not enough, you’ll see a new roommate, new love interests – and a new villain! Spider-Man goes back to basics courtesy of Nick Spencer (SECRET EMPIRE, SUPERIOR FOES OF SPIDER-MAN) and the Marvel debut of RYAN OTTLEY (Invincible)!</Text>
                    </View>
                </ScrollView>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        flex: 1
    },
    Header: {
        width: '100%',
        height: 44,
        backgroundColor: '#202020',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center'
    },
    content: {
        marginTop: 15,
        padding: 9,
    },
    title: {
        color: '#ffffff',
        fontSize: 26
    },
    imageContent: {
        marginTop: 10,
        width: 346,
        height: 532,
        alignSelf: 'center'
    },
    judulContent: {
        marginTop: 25,
        color: '#ffffff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    isi: {
        marginTop: 3,
        color: '#ffffff',
        fontSize: 16,
    },
    detail: {
        marginTop: 25,
        color: '#ffffff',
        fontSize: 15
    }

})

