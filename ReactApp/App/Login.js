import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ActivityIndicator,
    TextInput,
    TouchableOpacity
} from 'react-native';

export default class Login extends Component {
    state = {
        isError: false
    }

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            isError: false
        }
    }
    loginHandler() {
        if(this.state.username == 'rido' && this.state.password == '12345') {
            this.props.navigation.push('Home', {name: 'this.state.username'});
        } else {
            this.setState({ isError: true })
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.login}>
                    <Image source={require('../assets/logomarvellogin.png')} style={{ width: 200, height: 55 }} />
                </View>
                
                <View style={{ marginTop: 45 }}>
                {this.state.isError ?
                    <Text style={{alignSelf:'center', color:'red', fontSize: 14}}>
                        *Wrong Email or Password
                    </Text> :
                    false
                }
                    <View style={{ alignSelf: 'center' }}>
                        <TextInput
                            style={styles.input}
                            placeholder="Email Address"
                            onChangeText={username => this.setState({username})} 
                        />
                    </View>
                    <View style={{ alignSelf: 'center' }}>
                        <TextInput
                            style={styles.input}
                            placeholder="Password"
                            onChangeText={password => this.setState({password})} 
                        />
                    </View>
                    <TouchableOpacity
                        style={styles.loginbutton}
                        onPress={() => this.loginHandler()}> 
                        <Text style={{ fontSize: 14, color: '#fff', textAlign: 'center' }}>SIGN IN</Text>
                    </TouchableOpacity>
                    <View style={{ height: 1, backgroundColor: '#CCCCCC', width: 316, marginTop: 44, alignSelf: 'center' }} />
                    <TouchableOpacity
                        style={styles.RegisterButton}>
                        <Text style={{ fontSize: 14, color: '#000', textAlign: 'center' }}>CREATE AN ACCOUNT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    login: {
        paddingTop: 87,
        alignSelf: 'center'
    },
    input: {
        marginTop: 6,
        height: 48,
        width: 317,
        borderColor: '#CCCCCC',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 13,
        color: '#5E646B'
    },
    loginbutton: {
        marginTop: 25,
        height: 47,
        width: 317,
        backgroundColor: '#E62429',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    RegisterButton: {
        marginTop: 44,
        height: 47,
        width: 184,
        // backgroundColor: '#5c5c5c',
        alignSelf: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#5c5c5c'
    },
})

