import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Login from './Login'
import Home from './Home'
import Detail from './Detail'
import About from './About'

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
    <LoginStack.Navigator screenOptions={{
        headerShown: false
    }}>
        <LoginStack.Screen headerMode="none" name="Login" component={Login} options={{ title: "Login Page" }} />
    </LoginStack.Navigator>
)
const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator >
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="About Us" component={About} />
    </Drawer.Navigator>
)

const detailstack = createStackNavigator()
const detailscreen = () => (
    <detailstack.Navigator screenOptions={{
        headerShown: false
    }}>
        <detailstack.Screen name="detail" component={Detail} />
    </detailstack.Navigator>
)

const RootStack = createStackNavigator();
const RootStackScreen = () => (
    <RootStack.Navigator screenOptions={{headerShown: false}}>
        <RootStack.Screen name="Login" component={LoginStackScreen} />
        <RootStack.Screen headerMode="none" name="Home" component={DrawerScreen} />
        <RootStack.Screen headerMode="none" name="Detail" component={detailscreen} />
    </RootStack.Navigator>
)
export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>

);