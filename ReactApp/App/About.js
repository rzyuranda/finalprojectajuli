import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class About extends React.Component {
    render () {
        return(
            <View style={styles.container}>
            <ScrollView>
                <View style={styles.AtasBck}>
                    <Text style={styles.judul}>About US</Text>
                    {/* <Icon style={styles.navIcon} name="ios-contact" size={220} color="#EFEFEF" /> */}
                    <Text style={styles.nama}>Rezky Yuranda</Text>
                    <Text style={styles.nama}>Chandra Kirana</Text>
                    <Text style={styles.jobdesk}>React Native Developer</Text>
                </View>                
            </ScrollView>
        </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    AtasBck: {
        alignItems: 'center',
        marginTop: 150
    },
    judul: {
        marginTop: 64,
        fontWeight: 'bold',
        fontSize: 36,
        color: '#003366'
    },
    nama: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366'
    },
    jobdesk: {
        marginTop: 8,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF'
    },    

})