import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ActivityIndicator,
    TextInput,
    TouchableOpacity,
    ScrollView,
    FlatList
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
let numColumns = 3;

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        }
    }

    componentDidMount() {
        this.getListKomik()
    }

    getListKomik = async () => {
        try {
            const response = await Axios.get('https://gateway.marvel.com/v1/public/comics?format=comic&formatType=comic&noVariants=true&hasDigitalIssue=true&orderBy=focDate&ts=apalah&apikey=9140edca223de519a9af661015804662&hash=70a214dea2eb875a3b72836283cf7f58')
            this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }



    render() {

        if (this.state.isLoading) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <ActivityIndicator size='large' color='red' />
                    <Text style={{ marginTop: 4 }}>menunggu...</Text>
                </View>
            )
        } else if (this.state.isError) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text>Terjadi error saat memuat data</Text>
                </View>
            )
        }
        // alert(this.state.data.data.results[0]['creators']['items'][0]['name'])
        return (
            <View style={styles.container}>
                <View style={styles.Header}>
                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => toggleDrawer()}>
                        <Icon name="menu" size={25} color="#fff" style={{ marginLeft: 15 }} />
                    </TouchableOpacity>
                    <Image source={require('../assets/marvel-logo.png')} style={{ width: 122, height: 44, alignItems: 'center' }} />
                    <TouchableOpacity style={{ alignSelf: 'center' }}>
                        <Icon name="search" size={25} color="#fff" style={{ marginRight: 15 }} />
                    </TouchableOpacity>
                </View>
                {/* <View style={styles.content}> */}

                <FlatList
                contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}
                    numColumns={numColumns}
                    data={this.state.data.data.results}
                    renderItem={({ item }) =>
                        <View style={styles.content}>
                            <TouchableOpacity onPress={() => this.props.navigation.push('Detail',{id: item.title})}>
                                <View style={styles.boxContent}>
                                    <Image source={{ uri: `${item.thumbnail['path']+'.'+item.thumbnail['extension']}` }} style={{ width: 163, height: 244 }} />
                                    <Text style={{ fontWeight: 'bold', fontSize: 14, padding: 3, marginTop: 5 }}>{item.title}</Text>
                                    <Text style={{ fontSize: 11, padding: 3, marginTop: -5 }}>{item.dates.date}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    keyExtractor={({ id }, index) => index}
                />                
            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    Header: {
        width: '100%',
        height: 44,
        backgroundColor: '#202020',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center'
    },
    content: {
        marginTop: 25,
        padding: 8,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        flexWrap: 'wrap'

    },
    boxContent: {
        width: 163,
        height: 290,
        backgroundColor: '#fff',
        marginTop: 12
    }
})

